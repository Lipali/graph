import ajs.printutils.Color;
import ajs.printutils.PrettyPrintTree;

class MultiNodeExample {
    public static void main(String[] args) {
        MultiNode<String> root = new MultiNode<>("AAA");
        root.addChildren(
                new MultiNode<>("BBB").addChildren(
                        new MultiNode<>("EE"),
                        new MultiNode<>("FFF").addChildren(
                                new MultiNode<>("GGG"),
                                new MultiNode<>("HHH").addChildren(
                                        new MultiNode<>("XXX"),
                                        new MultiNode<>("YYY")
                                )
                        )
                ),
                new MultiNode<>("CCC"),
                new MultiNode<>("DDD")
        );

        PrettyPrintTree<MultiNode<String>> prettyTree = new PrettyPrintTree<>(
                MultiNode::getChildren,
                MultiNode::getValue
        );
        prettyTree.display(root);
    }
}
